package br.projeto.knin.joptional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

class JOptionalTest {

    @Test
    @DisplayName("Cenário para operação \"de\" cujo objeto no parametro de entrada seja nulo. Deve levantar uma exceção")
    void operacaoDeComObjetoDeEntradaNulo() {

        Assertions
                .assertThrows
                        (
                                NullPointerException.class,
                                () -> JOptional.de(null)
                        );

    }

    @Test
    @DisplayName("Cenário para operação \"de\" cujo objeto no parametro de entrada não seja nulo")
    void operacaoDeComObjetoDeEntradaNaoNulo() {

        Assertions.assertNotNull(JOptional.de(1));

    }

    @Test
    @DisplayName("Cenário para operação \"anulavel\" cujo objeto no parametro de entrada seja nulo")
    void operacaoAnulavelComObjetoDeEntradaNulo() {

        Assertions.assertNotNull(JOptional.anulavel(null));

    }

    @Test
    @DisplayName("Cenário para operação \"anulavel\" cujo objeto no parametro de entrada não seja nulo")
    void operacaoAnulavelComObjetoDeEntradaNaoNulo() {

        Assertions.assertNotNull(JOptional.anulavel(1));

    }

    @Test
    @DisplayName("Cenário para operação \"vazio\" cujo objeto do parametro de entrada seja nulo")
    void operacaoVazioComObjetoNulo() {

        Assertions
                .assertTrue(JOptional.anulavel(null).vazio());

    }

    @Test
    @DisplayName("Cenário para operação \"vazio\" cujo objeto do parametro de entrada não nulo. Caso de objeto construído pela operação anulavel")
    void operacaoVazioComObjetoNaoNuloParaOperacaoDeConstrucaoAnulavel() {

        Assertions
                .assertFalse(JOptional.anulavel("Copo meio cheio").vazio());

    }

    @Test
    @DisplayName("Cenário para operação \"vazio\" cujo objeto do parametro de entrada não nulo. Caso de objeto construído pela operação de")
    void operacaoVazioComObjetoNaoNuloParaOperacaoDeConstrucaoDe() {

        Assertions
                .assertFalse(JOptional.de("Copo meio vazio").vazio());

    }

    @Test
    @DisplayName("Cen[ario para operação \"mapear\" cuja instancia seja nula")
    void operacaoMapearComInstanciaDeFuncaoNula() {

        Assertions
                .assertThrows
                        (
                                NullPointerException.class,
                                () ->
                                        JOptional
                                                .de(true)
                                                .mapear(null)
                        );

    }

    @Test
    @DisplayName("Cenário para operação \"mapear\" cujo objeto do parametro de entrada seja nulo. Caso novo objeto mapeado seja nulo")
    void operacaoMapearComObjetoNuloCasoNovoObjetoMapeadoSejaNulo() {

        final JOptional<Object> jOptional = JOptional.anulavel(null);

        Assertions.assertNotEquals(jOptional, jOptional.mapear(o -> null));

    }

    @Test
    @DisplayName("Cenário para operação \"mapear\" cujo objeto do parametro de entrada seja nulo. Caso novo objeto mapeado seja nulo")
    void operacaoMapearComObjetoNuloCasoNovoObjetoMapeadoNaoSejaNulo() {

        Assertions
                .assertTrue
                        (
                                JOptional
                                        .anulavel(null)
                                        .mapear(o -> 1)
                                        .vazio()
                        );

    }

    @Test
    @DisplayName("Cenário para operação \"mapear\" cujo objeto do parametro de entrada não seja nulo. Caso novo objeto mapeado não seja nulo")
    void operacaoMapearComObjetoNaoNuloCasoNovoObjetoMapeadoNaoSejaNulo() {

        Assertions
                .assertFalse
                        (
                                JOptional
                                        .anulavel("Eu só posso lhe mostrar a porta.\n" +
                                                "Você tem que atravessá-la.")
                                        .mapear(String::length)
                                        .vazio()
                        );

    }

    @Test
    @DisplayName("Cenário para operação \"mapear\" cujo objeto do parametro de entrada não seja nulo. Caso novo objeto mapeado seja nulo")
    void operacaoMapearComObjetoNaoNuloCasoNovoObjetoMapeadoSejaNulo() {

        Assertions
                .assertTrue
                        (
                                JOptional
                                        .de("Não pense que é capaz. Saiba que é.")
                                        .mapear(o -> null)
                                        .vazio()
                        );

    }

    @Test
    @DisplayName("Cenário para operação \"filtrar\" cujo instância da função seja nulo")
    void operacaoFiltrarCujoInstanciaDaFuncaoSejaNula() {

        Assertions.assertThrows
                (
                        NullPointerException.class,
                        () -> JOptional.de('M').filtrar(null)
                );

    }

    @Test
    @DisplayName("Cenário para operação \"filtrar\" cujo objeto do parametro de entrada seja nulo")
    void operacaoFiltrarCujoObjetoSejaNulo() {

        final JOptional<Object> jOptional = JOptional.anulavel(null);

        Assertions.assertEquals(jOptional, jOptional.filtrar(Objects::isNull));

    }

    @Test
    @DisplayName("Cenário para operação \"filtrar\" cujo objeto do parametro de entrada não seja nulo." +
            "Caso predicado seja verdadeiro")
    void operacaoFiltrarCujoObjetoNaoSejaNuloCasoPredicadoSejaVerdadeiro() {

        Assertions
                .assertFalse
                        (
                                JOptional
                                        .anulavel("Matrix")
                                        .filtrar(s -> s.contains("M"))
                                        .vazio()
                        );

    }

    @Test
    @DisplayName("Cenário para operação \"filtrar\" cujo objeto do parametro de entrada não seja nulo." +
            "Caso predicado seja falso")
    void operacaoFiltrarCujoObjetoNaoSejaNuloCasoPredicadoSejaFalso() {

        Assertions
                .assertTrue
                        (
                                JOptional
                                        .anulavel(10)
                                        .filtrar(integer -> integer < 10)
                                        .vazio()
                        );

    }

    @Test
    @DisplayName("Cenário para operação \"entao\" cujo objeto do parametro de entrada seja nulo.")
    void operacaoEntaoCujoObjetoSejaNulo() {

        final AtomicBoolean atomicBoolean = new AtomicBoolean(false);

        JOptional.nulo().entao(o -> atomicBoolean.set(true));

        Assertions
                .assertFalse(atomicBoolean.get());

    }

    @Test
    @DisplayName("Cenário para operação \"entao\" cujo objeto do parametro de entrada não seja nulo.")
    void operacaoEntaoCujoObjetoNaoSejaNulo() {

        final AtomicBoolean atomicBoolean = new AtomicBoolean(false);

        JOptional.de(1).entao(o -> atomicBoolean.set(true));

        Assertions
                .assertTrue(atomicBoolean.get());

    }

    @Test
    @DisplayName("Cenário para operação \"entao\" cujo objeto do parametro de entrada não seja nulo. Caso instancia de consumo seja nula")
    void operacaoEntaoCujoObjetoNaoSejaNuloCasoInstanciaDeConsumoSejaNula() {

        Assertions
                .assertThrows
                        (
                                NullPointerException.class,
                                () -> JOptional.anulavel(1).entao(null)
                        );

    }

    @Test
    @DisplayName("Cenário para operação \"seNaoEntao\" cujo objeto do parametro de entrada seja nulo")
    void operacaoSeNaoEntaoCujoObjetoEntradaSejaNulo() {

        final AtomicInteger atomicInteger = new AtomicInteger(0);

        JOptional
                .nulo()
                .seNaoEntao(() -> atomicInteger.addAndGet(2))
                .entao(o -> atomicInteger.addAndGet(1));

        Assertions.assertEquals(2, atomicInteger.get());

    }

    @Test
    @DisplayName("Cenário para operação \"seNaoEntao\" cujo objeto do parametro de entrada não seja nulo")
    void operacaoSeNaoEntaoCujoObjetoEntradaNaoSejaNulo() {

        final AtomicInteger atomicInteger = new AtomicInteger(0);

        JOptional
                .de("The Matrix")
                .seNaoEntao(() -> atomicInteger.addAndGet(1))
                .entao(o -> atomicInteger.addAndGet(o.length()));

        Assertions.assertEquals(10, atomicInteger.get());

    }

    @Test
    @DisplayName("Cenário para operação \"obter\" cujo objeto do parametro de entrada seja nulo")
    void operacaoObterCasoObjetoDeEntradaSejaNulo() {

        Assertions
                .assertThrows(NoSuchElementException.class, () -> JOptional.nulo().obter());

    }

    @Test
    @DisplayName("Cenário para operação \"obter\" cujo objeto do parametro de entrada não seja nulo")
    void operacaoObterCasoObjetoDeEntradaNaoSejaNulo() {

        final Character valor = 'J';

        Assertions
                .assertEquals(valor, JOptional.de(valor).obter());

    }

    @Test
    @DisplayName("Cenário para operação \"se não obter\" cujo objeto do parametro de entrada não seja nulo")
    void operacaoSeNaoObterCasoObjetoDeEntradaNaoSejaNulo() {

        final Character valor = 'O';

        Assertions
                .assertEquals(valor, JOptional.de(valor).seNaoObter('P'));

    }

    @Test
    @DisplayName("Cenário para operação \"se não obter\" cujo objeto do parametro de entrada nulo")
    void operacaoSeNaoObterCasoObjetoDeEntradaSejaNulo() {

        final Character valor = 'O';

        Assertions
                .assertEquals(valor, JOptional.anulavel(null).seNaoObter(valor));

    }

    @Test
    @DisplayName("Cenário para operação \"se não exceção\" cujo objeto do parametro de entrada nulo")
    void operacaoSeNaoExcecaoLevantada() {
        Assertions
                .assertThrows
                        (
                                IllegalArgumentException.class,
                                () ->
                                        JOptional
                                                .nulo()
                                                .seNaoExcecao(IllegalArgumentException::new)
                        );
    }

    @Test
    @DisplayName("Cenário para operação \"se não exceção\" cujo objeto do parametro de entrada nulo")
    void operacaoSeNaoExcecaoNaoLevantada() {
        Assertions
                .assertDoesNotThrow
                        (
                                () ->
                                        JOptional
                                                .de(1)
                                                .seNaoExcecao(IllegalArgumentException::new)
                        );
    }

    @Test
    @DisplayName("Cenário para operação \"se não exceção\" caso instancia nula de produtor")
    void operacaoSeNaoExcecaoCasoInstanciaNulaDeProdutor() {
        Assertions
                .assertThrows
                        (
                                NullPointerException.class,
                                () -> JOptional
                                        .de(1)
                                        .seNaoExcecao(null));
    }


    @Test
    @DisplayName("Cenário para operação \"se não exceção\" caso objeto produzido seja nulo")
    void operacaoSeNaoExcecaoCasoObjetoProduzidoSejaNulo() {
        Assertions
                .assertDoesNotThrow(() -> JOptional
                        .de(1)
                        .seNaoExcecao(() -> null));
    }

}