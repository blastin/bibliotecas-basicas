package br.projeto.knin.colecao;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Trabalhando com ideias
 */
public class ColecaoTest {

    @Test
    void testandoColecaoExistente() {

        final Collection<Integer> colecao = new ArrayList<>();

        colecao.add(7);

        colecao.add(2);

        colecao.add(2);

        colecao.removeIf(integer -> integer == 2);

        colecao.forEach(System.out::println);

        colecao.stream().sorted(Integer::compareTo);

    }
}
