package br.projeto.knin.jstream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class JStreamTest {

    @Test
    @DisplayName("Cenário para operação \"de\" cuja entrada é referência de um array de tamanho igual a 4")
    void operacaoDe() {

        final int quantidade = JStream.de(1, 2, 3, 4).quantidade();

        Assertions
                .assertEquals(4, quantidade);

    }

    @Test
    @DisplayName("Cenário para operação \"de\" cuja entrada é uma referência nula")
    void operacaoDeComEntradaSejaUmaReferenciaNula() {

        Assertions
                .assertThrows
                        (
                                NullPointerException.class,
                                () -> JStream.de((Integer[]) null)
                        );

    }

    @Test
    @DisplayName("Cenário para operação \"de\" cuja entrada é um objeto unitário")
    void operacaoDeComEntradaDeObjetoUnico() {

        Assertions
                .assertEquals(1, JStream.de("J").quantidade());

    }

    @Test
    @DisplayName("Cenário para operação \"filtrar\" cuja entrada è uma referência de um array de tramanho 2." +
            "Caso todos objetos passam pela função de predicado")
    void operacaoFiltrarComEntradaDeArrayDeTamanhoDoisCasoFuncaoDePredicadoValideTodosObjetos() {

        Assertions
                .assertEquals(2, JStream.de(0, 1).filtrar(integer -> integer > -1).quantidade());

    }

    @Test
    @DisplayName("Cenário para operação \"filtrar\" cuja entrada è uma referência de um array de tramanho 2." +
            "Caso todos objetos não passem pela função de predicado")
    void operacaoFiltrarComEntradaDeArrayDeTamanhoDoisCasoFuncaoDePredicadoNaoValideObjetos() {

        Assertions
                .assertEquals(0, JStream.de(0, 1).filtrar(integer -> integer > 1).quantidade());

    }

    @Test
    @DisplayName("Cenário para operação \"filtrar\" caso instância de predicado seja igual a nula")
    void operacaoFiltrarCasoInstanciaDePredicadoIgualNula() {

        Assertions
                .assertThrows
                        (
                                NullPointerException.class,
                                () -> JStream.de(1).filtrar(null)
                        );

    }

    @Test
    @DisplayName("Cenário para operação \"filtrar\" caso jstream esteja vazio")
    void operacaoFiltrarCasoJStreamEstejaVazio() {

        Assertions
                .assertEquals
                        (
                                0,
                                JStream
                                        .de(1)
                                        .filtrar(integer -> integer > 1)
                                        .filtrar(integer -> integer > 2)
                                        .quantidade()
                        );

    }

    @Test
    @DisplayName("Cenário para operação \"mapear\" caso mapeamento com sucesso")
    void operacaoMapearCasoMapeamentoComSucesso() {

        Assertions
                .assertEquals
                        (
                                1,
                                JStream
                                        .de('A')
                                        .mapear(Character::getNumericValue)
                                        .quantidade()
                        );
    }

    @Test
    @DisplayName("Cenário para operação \"mapear\" caso mapeamento retorne null")
    void operacaoMapearCasoMapeamentoRetorneNull() {

        Assertions
                .assertEquals
                        (
                                0,
                                JStream
                                        .de('A')
                                        .mapear(character -> null)
                                        .quantidade()
                        );
    }

    @Test
    @DisplayName("Cenário para operação \"mapear\" caso função seja nula")
    void operacaoMapearCasoFuncaoSejaNula() {

        Assertions
                .assertThrows
                        (
                                NullPointerException.class,
                                () -> JStream
                                        .de('A')
                                        .mapear(null)
                        );
    }

    @Test
    @DisplayName("Cenário para operação \"reduzir\" realizada com sucesso")
    void operacaoReduzirRealizadaComSucesso() {

        Assertions
                .assertEquals
                        (55,
                                JStream
                                        .de(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                                        .reduzir(0, Integer::sum)
                                        .seNaoObter(0)
                        );
    }

    @Test
    @DisplayName("Cenário para operação \"reduzir\" caso stream vazio")
    void operacaoReduzirCasoStreamVazio() {

        Assertions
                .assertEquals
                        (-1,
                                JStream
                                        .de(29, 30)
                                        .filtrar(integer -> integer > 30)
                                        .reduzir(0, Integer::sum)
                                        .seNaoObter(-1)
                        );
    }

    @Test
    @DisplayName("Cenário para operação \"reduzir\" caso instância de filtro nula")
    void operacaoReduzirCasoInstanciaDeFiltroNula() {

        Assertions
                .assertThrows
                        (
                                NullPointerException.class,
                                () -> JStream
                                        .de('A')
                                        .filtrar(null)
                        );
    }

    @Test
    @DisplayName("Cenário para operação \"reduzir\" caso instância de semente nula")
    void operacaoReduzirCasoInstanciaDeSementeNula() {

        Assertions
                .assertThrows
                        (
                                NullPointerException.class,
                                () -> JStream
                                        .de("J")
                                        .reduzir(null, String::concat)
                        );
    }

    @Test
    @DisplayName("Cenário para operação \"reduzir\" caso instância de operador nulo")
    void operacaoReduzirCasoInstanciaDeOperadorNula() {

        Assertions
                .assertThrows
                        (
                                NullPointerException.class,
                                () -> JStream
                                        .de("J")
                                        .reduzir("  ", null)
                        );
    }

    @Test
    @DisplayName("Cenário para operação \"encontrar primeiro\" caso exista objeto a ser retornado")
    void operacaoEncontrarPrimeiro() {

        final String filme = "The Matrix";

        Assertions
                .assertEquals
                        (
                                filme,
                                JStream
                                        .de(filme)
                                        .encontrarPrimeiro()
                                        .seNaoObter("")
                        );
    }

    @Test
    @DisplayName("Cenário para operação \"encontrar primeiro\" caso não exista objeto a ser retornado")
    void operacaoEncontrarPrimeiroCasoNaoExistaObjeto() {

        Assertions
                .assertEquals
                        (
                                0,
                                JStream
                                        .de(1)
                                        .filtrar(integer -> integer > 2)
                                        .encontrarPrimeiro()
                                        .seNaoObter(0)
                        );
    }

    //FIXME: ts não pode ser utilzado para teste. Implemente coleção
    @Test
    @DisplayName("Cenário para operação \"ordenar\" caso de sucesso")
    void operacaoOrdenar() {

        final Integer[] numeros = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        Assertions
                .assertArrayEquals
                        (
                                numeros,
                                JStream
                                        .de(10, 8, 9, 7, 4, 5, 6, 1, 2, 3)
                                        .ordenar(Integer::compareTo)
                                        .ts
                        );

    }

    @Test
    @DisplayName("Cenário para operação \"ordenar\" caso o fluxo esteja vazio")
    void operacaoOrdenarCasoFluxoEstejaVazio() {

        Assertions
                .assertEquals
                        (
                                0,
                                JStream
                                        .de(20, 10)
                                        .filtrar(integer -> integer > 20)
                                        .ordenar(Integer::compareTo)
                                        .quantidade()
                        );

    }

    @Test
    @DisplayName("Cenário para operação \"ordenar\" caso o comparador nulo")
    void operacaoOrdenarCasoComparadorNulo() {

        Assertions
                .assertThrows
                        (
                                NullPointerException.class,
                                () ->
                                        JStream
                                                .de(1)
                                                .ordenar(null)
                        );
    }

}