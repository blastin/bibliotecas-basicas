package br.projeto.knin.joptional;

import br.projeto.knin.padrao.*;

import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * <p>
 * JOptional é uma reescrita do Optional encontrado na biblioteca padrão da jdk.
 * Tem como proposito analítico, compreender detalhes de implementação e aprofundar conhecimento em tipos géricos
 * na linguagem de programação java.
 *
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 * @implNote JOptional é uma classe de tipo genérico <T> com alguns métodos de operações básicas como
 * 1. Mapeamento
 * 2. Filtro
 * 3. Conteudo Vazio
 * 4. Ação Caso exista
 * 5. Ação Caso não exista
 * 6. Obter
 * 7. Se não obter
 * 8. Se não exceção
 */
public class JOptional<T> {

    /**
     * @param t   argumento de entrada para função estática de construção.
     *            Caso o objeto seja nulo, uma exceção **NullPointerException** será levantada
     * @param <T> tipo genérico inferido
     * @return uma instância de JOptional do tipo <T>
     */
    public static <T> JOptional<T> de(final T t) {
        Objects.requireNonNull(t);
        return new JOptional<>(t);
    }

    /**
     * Função específica quando existe a possibilidade de um objeto ser nulo
     *
     * @param t   argumento de entrada para função estática de construção.
     *            Caso o objeto seja nulo, nada ocorrerá.
     * @param <T> tipo genérico inferido
     * @return uma instância de JOptional do tipo <T>
     */
    public static <T> JOptional<T> anulavel(final T t) {
        return new JOptional<>(t);
    }

    /**
     * Evitando criar uma nova instância quando ocorre um cenário de objeto nulo.
     * Uma instância cacheada será devolvida de acordo com a definição do tipo de objeto de saída feita pelo método
     *
     * @param <E> tipo genérico inferido
     * @return objeto cast do tipo <E>
     */
    @SuppressWarnings("unchecked")
    public static <E> JOptional<E> nulo() {
        return (JOptional<E>) NULO;
    }

    private static final JOptional<?> NULO = anulavel(null);

    private JOptional(final T t) {
        this.t = t;
    }

    private final T t;

    /**
     * @return caso objeto t do tipo <T> seja nulo, retornará true, caso contrário false
     */
    public boolean vazio() {
        return t == null;
    }

    /**
     * @param funcao instância de um objeto de função de tipo <T,E>.
     *               <T> é parâmetro do tipo da classe JOptional
     *               <E> parâmetro desejado de saída
     *               Caso a instância seja nula, uma exceção NullPointerException será levantada
     * @param <E>    tipo genérico inferido
     * @return uma instância de JOptinal do tipo <E>
     */
    public <E> JOptional<E> mapear(final Funcao<? super T, ? extends E> funcao) {

        Objects.requireNonNull(funcao);

        if (vazio()) return nulo();

        return anulavel(funcao.aplicar(t));

    }

    /**
     * @param predicado instância de um objeto de função de tipo T, boolean
     *                  T é parâmetro do tipo da classe JOptional
     *                  Caso a instância seja nula, uma exceção NullPointerException será levantada
     * @return caso predicado retorne verdadeiro, a mesma instância será retornada, caso contrário uma instância
     * com objeto nulo
     */
    public JOptional<T> filtrar(final Predicado<? super T> predicado) {

        Objects.requireNonNull(predicado);

        if (vazio()) return this;

        return predicado.test(t) ? this : nulo();

    }

    /**
     * Operação 'então' é utilizada para consumir um objeto caso ele não seja nulo
     *
     * @param consumidor instância de um objeto de consumidor de tipo T
     *                   T é um parâmetro do tipo da classe JOptional
     *                   Caso a instância seja nula, uma exceção NullPointerException será levantada
     */
    public void entao(final Consumidor<? super T> consumidor) {

        Objects.requireNonNull(consumidor);

        if (!vazio()) consumidor.utilizar(t);

    }

    /**
     * Operação 'se não então' é utilizada para tomar uma ação caso o objeto seja nulo
     *
     * @param acao instância de um objeto de ação
     *             T é um parâmetro do tipo da classe JOptional
     *             Caso a instância seja nula, uma exceção NullPointerException será levantada
     * @return instância JOptional sem efeito colateral
     */
    public JOptional<T> seNaoEntao(final Acao acao) {

        Objects.requireNonNull(acao);

        if (vazio()) acao.executar();

        return this;

    }

    /**
     * Operação 'obter' é utilizada para obter instância encapsulada pelo JOptional do tipo T.
     * Apenas será recuperado caso não seja nulo
     *
     * @return caso objeto não seja nulo, retorna a objeto do tipo T.
     * Caso contrário uma exceção NoSuchElementException será levantada
     */
    public T obter() {

        if (vazio()) throw new NoSuchElementException("Objeto nulo");

        return t;

    }

    /**
     * Operação 'se não obter' é utilizada com alternativa de valor caso objeto encapsulado seja nulo
     *
     * @param outro objeto do tipo T
     * @return outro é retornado caso o objeto encapsulado seja nulo, caso contrário é retornado o objeto encapsulado
     */
    public T seNaoObter(final T outro) {

        if (vazio()) return outro;

        return t;

    }

    /**
     * Operação 'se não exceção' é executada quando fluxo de objetos se encontra vázio. Uma exeção pode ser levantada para
     * concretezar uma falha de fluxo
     *
     * @param produtor instância da interface Produtor do tipo inferido pelo método
     *                 Caso a instância seja nula, uma exceção NullPointerException será levantada
     * @param <E>      E representa um subtipo de Throwable
     * @throws E Exceção do tipo E será levantada caso o objeto produzido seja do tipo Throwable
     */
    public <E extends Throwable> JOptional<T> seNaoExcecao(final Produtor<E> produtor) throws E {

        Objects.requireNonNull(produtor);

        if (!vazio()) return this;

        throw produtor.produzir();

    }

}
