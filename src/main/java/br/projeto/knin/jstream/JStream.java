package br.projeto.knin.jstream;

import br.projeto.knin.joptional.JOptional;
import br.projeto.knin.padrao.Comparador;
import br.projeto.knin.padrao.Funcao;
import br.projeto.knin.padrao.Operador;
import br.projeto.knin.padrao.Predicado;
import br.projeto.knin.utils.MergeSort;

import java.util.Objects;

/**
 * @author Jefferson Lisboa de Souza (lisboa.jeff@gmail.com)
 * @implSpec JStream é uma reimplementação da biblioteca básica Stream encontrada no jdk.
 * Seu objetivo é construir um fluxo de dados para lidar com uma JCollection.
 * <p>
 * As seguintes operações são encontrada
 * <p>
 * 1. Mapeamento
 * 2. Filtragem
 * 3. Redução
 * 4. Encontrar Primeiro Objeto
 * 4. Coleção
 * 5. Ordenação
 * <p>
 * JStream é do tipo genérico T
 */
public class JStream<T> {

    /**
     * Função de construção de JStream do tipo T estática. Um array de objetos do tipo T é inserido, assim como
     * o quantidade do array.
     *
     * @param ts  referência a um array de objetos do tipo T
     * @param <T> tipo genérico inferido pelo operação 'de'
     * @return uma nova instância JStream do tipo inferido pelo método. Caso ts seja nulo, será levantado
     * uma exceção NullPointerException
     */
    @SafeVarargs
    public static <T> JStream<T> de(final T... ts) {
        Objects.requireNonNull(ts);
        return de(ts.length, ts);
    }

    /**
     * Função privada de construção de JStream do tipo T estática.
     *
     * @param quantidade quantidade de objetos para fluxo de dados
     * @param ts         referência de array de objetos do tipo T
     * @param <T>        tipo T inferido no método
     * @return uma nova instância JStream do tipo inferido pelo método
     */
    @SafeVarargs
    private static <T> JStream<T> de(final int quantidade, final T... ts) {
        return new JStream<>(ts, quantidade);
    }

    /**
     * Operação estática para realizar um cast em uma instância de array de quantidade i
     *
     * @param quantidade quantidade total necessário
     * @param <E>        tipo inferido pelo método
     * @return referência de array do tipo E
     */
    @SuppressWarnings("unchecked")
    private static <E> E[] novoArray(final int quantidade) {
        return (E[]) new Object[quantidade];
    }

    private static final JStream<?> NULO = de(0, (Object[]) null);

    /**
     * Operação estática para realizar um cast em um objeto cache de JStream do tipo E
     *
     * @param <E> tipo genérico inferido pelo método
     * @return instância NULO com cast
     */
    @SuppressWarnings("unchecked")
    private static <E> JStream<E> nulo() {
        return (JStream<E>) NULO;
    }

    private JStream(final T[] ts, final int quantidade) {
        this.ts = ts;
        this.quantidade = quantidade;
    }

    @Deprecated(forRemoval = true, since = "ts não pode ter visibilidade protegido por pacote")
    final T[] ts;

    private final int quantidade;

    /**
     * @return quantidade de dados inseridos no fluxo de dados
     */
    public int quantidade() {
        return quantidade;
    }

    /**
     * Filtrar é uma operação lógica que executa para cada objeto uma função de predicado
     *
     * @param predicado instância de predicado do tipo T da classe JStream.
     *                  Caso seja nulo será levantado uma exceção NullPointerException
     * @return instância JStream do tipo T com objetos filtrados. Caso nenhum objeto esteja enquadrado no predicado,
     * uma instância vazia será retornada
     */
    public JStream<T> filtrar(final Predicado<? super T> predicado) {

        Objects.requireNonNull(predicado);

        return aplicar(predicado, t -> t);

    }

    /**
     * Mapear é uma operação que mapeadora que executa para cada objeto uma transformação A -> B
     *
     * @param funcao instância de função do tipo T,E
     *               Caso seja nulo será levantado uma exceção NullPointerException
     * @param <E>    tipo genérico inferido pelo método
     * @return uma nova instância de JStream do tipo E. Caso nenhum objeto esteja enquadrado no predicado,
     * uma instância vazia será retornada
     */
    public <E> JStream<E> mapear(final Funcao<? super T, ? extends E> funcao) {

        Objects.requireNonNull(funcao);

        return aplicar(Objects::nonNull, funcao);

    }

    /**
     * Operação de redução do fluxo de dados em torno de um operador
     *
     * @param semente  valor inicial
     *                 Caso seja nulo, levantará uma exceção NullPointerException
     * @param operador instância de um operador. Caso seja nulo, levantará uma exceção NullPointerException
     * @return uma nova instância de JOptional do tipo T definido pela classe JStream
     */
    public JOptional<T> reduzir(final T semente, final Operador<? super T, ? extends T> operador) {

        Objects.requireNonNull(operador);

        Objects.requireNonNull(semente);

        if (vazio()) return JOptional.nulo();

        T agregador = semente;

        for (int i = 0; i < quantidade; i++) {
            agregador = operador.operar(agregador, ts[i]);
        }

        return JOptional.anulavel(agregador);

    }

    /**
     * Operação `encontrar primeiro` encapsula objeto do indice `0` em JOptional do tipo genérico
     * da classe JStream
     *
     * @return primeiro valor possível caso exista
     */
    public JOptional<T> encontrarPrimeiro() {
        return vazio() ? JOptional.nulo() : JOptional.anulavel(ts[0]);
    }

    /**
     * Aplicar é uma operação privada cujo proposito é aplicar uma função e executar uma ação de acordo com um predicado
     *
     * @param predicado instância de um predicado do tipo E
     * @param funcao    instância de uma função do tipo T,E
     * @param <E>       tipo genérico inferido pelo método
     * @return caso não esteja vazio, retorna uma nova instância de JStream do tipo E. Caso contrário retorna um objeto
     * cache castiado
     */
    private <E> JStream<E> aplicar(final Predicado<? super E> predicado, final Funcao<? super T, ? extends E> funcao) {

        if (vazio()) return nulo();

        final E[] tsAplicado = novoArray(quantidade);

        int j = 0;

        for (int i = 0; i < quantidade; i++) {

            final E e = funcao.aplicar(ts[i]);

            if (predicado.test(e)) tsAplicado[j++] = e;

        }

        return de(j, tsAplicado);

    }

    /**
     * Operação privada que valida a existência de pelo menos um objeto guardado
     *
     * @return true para quantidade == 0, caso contrário false
     */
    private boolean vazio() {
        return quantidade == 0;
    }

    /**
     * Operação ordenar facilita a ordenação do fluxo de objetos a partir de uma interface Comparador
     *
     * @param comparador instância de um Comparador do tipo T da classe JStream
     *                   caso comparador seja nulo, uma exceção NullPointerException será levantada.
     * @return uma nova instância de JStream com objetos ordenados
     * caso contrário a mesma instância de JStream com fluxo de objetos vazio
     */
    public JStream<T> ordenar(final Comparador<? super T> comparador) {

        Objects.requireNonNull(comparador);

        if (vazio()) return this;

        final T[] copia = novoArray(quantidade);

        System.arraycopy(ts, 0, copia, 0, quantidade);

        MergeSort.ordenar(copia, 0, quantidade - 1, comparador);

        return JStream.de(copia);

    }

}
