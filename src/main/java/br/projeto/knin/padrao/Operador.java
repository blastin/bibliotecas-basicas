package br.projeto.knin.padrao;

/**
 * @param <E> tipo genérico da interface. Representam os valores de entrada
 * @param <S> tipo genérico da interface. Representa o valor de saida
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 * @implSpec Operador do tipo T é uma interface com proposito de reduzir uma informação em torno de um valor inicial (semente)
 * e próximo valor a ser operado
 */
@FunctionalInterface
public interface Operador<E, S> {

    S operar(final E a, final E b);

}
