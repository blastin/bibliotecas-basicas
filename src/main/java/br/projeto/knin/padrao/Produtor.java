package br.projeto.knin.padrao;

/**
 * Interface Produtor facilita comportamentos que necessitam de uma operação que retorna um objeto, porém,
 * não possuem um argumento de entrada
 *
 * @param <T> tipo genérico do tipo T
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
@FunctionalInterface
public interface Produtor<T> {

    T produzir();

}
