package br.projeto.knin.padrao;

/**
 * Interface função que aplica uma função a um dado objeto do tipo <T> e retorna um objeto do tipo <E>.
 * Uma reescrita da interface funcional <<Function>> já encontrada na jdk do java
 *
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
@FunctionalInterface
public interface Funcao<T, E> {

    E aplicar(final T t);

}
