package br.projeto.knin.padrao;

/**
 * Função cujo proposito seja retornar um valor booleano a partir de um operador lógico.
 * Predicado possui algumas operações de predicado para processar um circuito lógico
 *
 * @param <T> genérico do tipo T
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
@FunctionalInterface
public interface Predicado<T> {

    boolean test(final T t);

}
