package br.projeto.knin.padrao;

/**
 * Interface Comparável do tipo genérico T
 * <p>
 * Proposito de realizar comparação de dois objetos de acordo com sua ordem natural
 * <p>
 *
 * @param <T> tipo genérico da interface
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface Comparador<T> {

    int compare(final T a, final T b);

}
