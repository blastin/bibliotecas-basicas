package br.projeto.knin.padrao;

/**
 * Interface funcional para executar uma ação
 *
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
@FunctionalInterface
public interface Acao {

    void executar();

}
