package br.projeto.knin.padrao;

/**
 * Interface funcional Consumo do tipo T.
 * Facilita idealizar o comportamento de uma operação que possui um objeto
 * como argumento de entrada, mas não retornam um objeto
 *
 * @param <T> tipo gênerico da interface
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
@FunctionalInterface
public interface Consumidor<T> {

    void utilizar(final T t);

}
