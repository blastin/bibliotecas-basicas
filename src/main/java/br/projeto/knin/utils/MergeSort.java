package br.projeto.knin.utils;

import br.projeto.knin.padrao.Comparador;

public class MergeSort {

    public static <T> void ordenar(final T[] objetos, final int posicaoInicio, final int posicaoFim, final Comparador<T> comparador) {

        if (posicaoInicio == posicaoFim) return;

        final int metadeTamanho = (posicaoInicio + posicaoFim) / 2;

        ordenar(objetos, posicaoInicio, metadeTamanho, comparador);

        ordenar(objetos, metadeTamanho + 1, posicaoFim, comparador);

        int i = posicaoInicio;

        int j = metadeTamanho + 1;

        int k = 0;

        @SuppressWarnings("unchecked") final T[] arrayTemporario = (T[]) new Object[posicaoFim - posicaoInicio + 1];

        while (i < metadeTamanho + 1 || j < posicaoFim + 1) {

            if (i == metadeTamanho + 1) {

                arrayTemporario[k] = objetos[j];

                j++;

            } else {

                if (j == posicaoFim + 1) {

                    arrayTemporario[k] = objetos[i];

                    i++;

                } else {

                    if (comparador.compare(objetos[i], objetos[j]) >= 0) {

                        arrayTemporario[k] = objetos[j];

                        j++;

                    } else {

                        arrayTemporario[k] = objetos[i];

                        i++;

                    }
                }
            }

            k++;

        }

        if (posicaoFim + 1 - posicaoInicio >= 0)
            System.arraycopy(arrayTemporario, 0, objetos, posicaoInicio, posicaoFim + 1 - posicaoInicio);

    }

    private MergeSort() {
    }

}
